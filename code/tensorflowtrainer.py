from lxml import html
import requests
import json
import os
import numpy as np
from numpy import arange
from parse import *
import parse
import re
from datetime import datetime
import time
import numpy as np
from json import dumps, load
from datetime import timedelta
from collections import OrderedDict
from sknn.mlp import Regressor, Layer
from pre_process_helper import normalize_features
import os.path
import pickle
import matplotlib
matplotlib.use('Agg')
from matplotlib.pyplot import *
import tensorflow as tf

#load the json file
filepath = '/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/data/' + time.strftime("%Y_%m_%d") + '.json'
fin = open(filepath,'r')

#get the json contents in an object
contents = json.load(fin)

#get the input, X and output, Y for the network
train_X, train_Y_avail, train_Y_price = normalize_features(contents)

#convert these lists to arrays
train_X = np.array(train_X)
train_Y_avail = np.array(train_Y_avail)
train_Y_price = np.array(train_Y_price)

train_Y_avail = np.reshape(train_Y_avail, [-1,1])


with tf.Graph().as_default():
    x = tf.placeholder("float", shape=[None, 8])
    y_ = tf.placeholder("float", shape=[None, 1])

    #W1 = tf.Variable(tf.random_normal([8,100], stddev=1), name="weights1")
    W1 = tf.Variable(tf.zeros([8,100]), name="weights1")
    b1 = tf.Variable(tf.zeros([100]))
    #W2 = tf.Variable(tf.random_normal([100,1], stddev=1), name="weights2")
    W2 = tf.Variable(tf.zeros([100,20]), name="weights2")
    b2 = tf.Variable(tf.zeros([20]))
    W3 = tf.Variable(tf.zeros([20,20]), name="weights3")
    b3 = tf.Variable(tf.zeros([20]))
    W4 = tf.Variable(tf.zeros([20,1]), name="weights4")
    b4 = tf.Variable(tf.zeros([1]))

    sess = tf.Session()
    sess.run(tf.initialize_all_variables())

    h_fc1 = tf.nn.sigmoid(tf.matmul(x, W1) + b1)
    h_fc2 = tf.nn.sigmoid(tf.matmul(h_fc1, W2) + b2)
    h_fc3 = tf.nn.sigmoid(tf.matmul(h_fc2, W3) + b3)
    y = tf.matmul(h_fc3, W4) + b4

    mse_loss = tf.nn.l2_loss(y-y_)
    mse_summary = tf.scalar_summary('mse',  mse_loss)
    #sparsity_mse = tf.scalar_summary('mse-sparsity', tf.nn.zero_fraction(mse_loss))

    hist_summary1 = tf.histogram_summary('weight1', W1)
    hist_summary2 = tf.histogram_summary('weight2', W2)
    hist_summary3 = tf.histogram_summary('weight3', W3)
    hist_summary4 = tf.histogram_summary('weight4', W4)
    #sparse1 = tf.histogram_summary('sparsity1', tf.nn.zero_fraction(W1))
    #sparse2 = tf.histogram_summary('sparsity2', tf.nn.zero_fraction(W2))
    

    merged_summary_op = tf.merge_all_summaries(key='summaries')
    summary_writer = tf.train.SummaryWriter('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/logs', graph_def=sess.graph_def)
    
    train_step = tf.train.GradientDescentOptimizer(0.00001).minimize(mse_loss)
    
    feed_dict={x: train_X, y_: train_Y_avail}

    for i in range(1000):
        sess.run(train_step, feed_dict=feed_dict)
        print i
        if (i%10 == 0):
            summary_str = sess.run(mse_summary, feed_dict=feed_dict)
            summary_writer.add_summary(summary_str, i)