# -*- coding: utf-8 -*-
"""
Created on Sun Oct 04 22:52:29 2015

@author: Ashish
"""

from lxml import html
import requests
import json
import os
import numpy as np
from numpy import arange
from parse import *
import parse
import re
from datetime import datetime
import time
import numpy as np
from json import dumps, load
from datetime import timedelta
from collections import OrderedDict
from sknn.mlp import Regressor, Layer
from pre_process_helper import normalize_features
import os.path
import pickle
import matplotlib
matplotlib.use('Agg')
from matplotlib.pyplot import *

#load the json file
filepath = '/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/data/' + time.strftime("%Y_%m_%d") + '.json'
fin = open(filepath,'r')

#get the json contents in an object
contents = json.load(fin)

#get the input, X and output, Y for the network
train_X, train_Y_avail, train_Y_price = normalize_features(contents)

#convert these lists to arrays
train_X = np.array(train_X)
train_Y_avail = np.array(train_Y_avail)
train_Y_price = np.array(train_Y_price)

#NOW WE HAVE THE INPUT AND OUTPUT DATA FOR TRAINING THE NETWORK

#NEURAL=NETWORK=PART=BEGINS====================================================

#=================1. AVAILABILITY ========================================
#for the first time, create a new network
if (not(os.path.exists('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_avail.pkl'))):
    nn_avail = Regressor(
        layers=[
            Layer("Tanh", units = 20),
            Layer("Tanh", units = 10),
            Layer("Tanh", units = 7),
            Layer("Tanh", units = 7),
            Layer("Tanh", units = 1),
            Layer("Linear")],
        learning_rate=0.01,
        n_iter=100)
else:
    nn_avail = pickle.load(open('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_avail.pkl','rb'))

    
#now train the network
nn_avail.fit(train_X[0:-1000], train_Y_avail[0:-1000])

#now test the network
test_X = train_X[-1000:]
test_Y_avail = train_Y_avail[-1000:]
pred_Y_avail = nn_avail.predict(test_X)

#reshape predictions to (1000,) from (1000,1)
pred_Y_avail = np.reshape(pred_Y_avail, (1000,))

print ("loss = ", nn_avail.score(test_X, test_Y_avail))

figure
plot(test_Y_avail, 'b')
plot(pred_Y_avail, 'r')
savefig('test2.png')
clf()

#save the network
pickle.dump(nn_avail, open('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_avail.pkl', 'wb'))

#=========================================================================

"""

#=================2. PRICE ===============================================

#for the first time, create a new network
if (not(os.path.exists('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_price.pkl'))):
    nn_price = Regressor(
        layers=[
            Layer("Sigmoid", units = 100),
            Layer("Linear", units = 100),
            Layer("Linear")],
        learning_rate=0.001,
        n_iter=1000
else:
    nn_price = pickle.load(open('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_price.pkl','rb'))

#now train the network
nn_price.fit(train_X[0:-100], train_Y_price[0:-100])

#now test the network
pred_Y_price = nn_price.predict(train_X[-100:])

#plot
#plot (pred_Y_price, 'ro')
#plot(train_Y_price[-100:])
#show()   

#save the network
pickle.dump(nn_price, open('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_price.pkl', 'wb'))
#=========================================================================


    

"""                
                      
    




    