# -*- coding: utf-8 -*-
"""
Created on Sun Oct 04 22:52:29 2015

@author: Ashish
"""

from lxml import html
import requests
import json
import os
import numpy as np
from numpy import arange
from parse import *
import parse
import re
from datetime import datetime
import time
import numpy as np
from json import dumps, load
from datetime import timedelta
from collections import OrderedDict
from sknn.mlp import Regressor, Layer


def travelday(daynum):
    l = [0,0,0,0,0,0,0]
    l[daynum-1] = 1
    return l

def travelmonth(monthnum):
    m = [0,0,0,0,0,0,0,0,0,0,0,0]
    m[monthnum-1] = 1
    return m

def seatsleft(seatnum):
    s = [0,0,0,0,0,0,0,0]
    s[seatnum-1] = 1
    return s


    