# -*- coding: utf-8 -*-
"""
Created on Sun Oct 04 22:52:29 2015

@author: Ashish
"""

from lxml import html
import requests
import json
import os
import numpy as np
from numpy import arange
from parse import *
import parse
import re
from datetime import datetime
import time
import numpy as np
from json import dumps, load
from datetime import timedelta
from collections import OrderedDict
from sknn.mlp import Regressor, Layer
from onehot import travelday, travelmonth, seatsleft


def normalize_features_with_dummy_encoding(contents):
    #get the number of result rows
    resultLen = len(contents)

    #initialize the two lists train_X and train_Y_price, train_Y_avail
    train_X = []
    train_Y_price = []
    train_Y_avail = []

    #start a loop over resultLen number of results to (i) convert all numbers from -1 to -1 and (ii) store them in train_X & train_Y
    for i in range(resultLen):
        #get the i^th result vector
        result = contents[i]
    
        #============feature=one===travel=day======================================        
        feat1 = travelday(result['travelDay'])
        #==========================================================================
    
        #==========feature=two=====travel=month====================================
        feat2 = travelmonth(result['travelMonth'])
        #==========================================================================
        
        #=========feature=three====source=latitude=================================
        MAX_LATITUDE = 71.5
        MIN_LATITUDE = 27.0    
        #sources : (i)https://www.google.de/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=lisbon%20longitude%20and%20latitude
        #sources : (ii)http://earthquake.usgs.gov/hazards/apps/vs30/predefined.php  *************NICE*************
        feat3 = [2 * (float(result['sourceLatitude'] - MIN_LATITUDE)/(MAX_LATITUDE-MIN_LATITUDE)) - 1]
        #==========================================================================

        #=========feature=four====source=longitude=================================
        MAX_LONGITUDE = 42.5
        MIN_LONGITUDE = -10.0
        feat4 = [2 * (float(result['sourceLongitude'] - MIN_LONGITUDE)/(MAX_LONGITUDE - MIN_LONGITUDE)) - 1]
        #==========================================================================
        
        #=========feature=five======destination=latitude===========================
        feat5 = [2 * (float(result['destinationLatitude'] - MIN_LATITUDE)/(MAX_LATITUDE - MIN_LATITUDE)) - 1]
        #==========================================================================
    
        #=========feature=six======destination=longitude===========================
        feat6 = [2 * (float(result['destinationLongitude'] - MIN_LONGITUDE)/(MAX_LONGITUDE - MIN_LONGITUDE)) - 1]
        #==========================================================================
    
        #=========feature=seven====search=travel=timestamp=difference==============
        MAX_TIMESTAMP_DIFFERENCE = 15552000.0
        #this corresponds to six months in terms of seconds
        feat7 = [2 * (float(result['travelTimestamp']-result['searchTimestamp'])/MAX_TIMESTAMP_DIFFERENCE) - 1]
        #==========================================================================
    
        #=========feature=eight======seats=left====================================
        feat8 = seatsleft(result['seatsLeft'])
        #==========================================================================
        
        #========output=one=======availablility====================================
        out1 = result['resultAvailability']
        #==========================================================================
        
        #========output=two==========price=========================================
        out2 = result['resultPrice']
        #==========================================================================
    
    
    
        #============append=to=train_X=train_Y_avail=train_Y_price=================
        train_X.append(feat1+feat2+feat3+feat4+feat5+feat6+feat7+feat8)
        #train_X is of length 32
        train_Y_avail.append(out1)
        train_Y_price.append(out2)
        #==========================================================================

        
    #return the input X, and the two kind of outputs/ Y's
    return train_X, train_Y_avail, train_Y_price


def normalize_features(contents):
    #get the number of result rows
    resultLen = len(contents)

    #initialize the two lists train_X and train_Y_price, train_Y_avail
    train_X = []
    train_Y_price = []
    train_Y_avail = []

    #start a loop over resultLen number of results to (i) convert all numbers from -1 to -1 and (ii) store them in train_X & train_Y
    for i in range(resultLen):
        #get the i^th result vector
        result = contents[i]
    
        #============feature=one===travel=day======================================
        MAX_TRAVEL_DAY = 7.0
        MIN_TRAVEL_DAY = 1.0
        feat1 = 2 * (float(result['travelDay']-MIN_TRAVEL_DAY)/(MAX_TRAVEL_DAY-MIN_TRAVEL_DAY)) - 1
        #==========================================================================
    
        #==========feature=two=====travel=month====================================
        MAX_TRAVEL_MONTH = 12.0
        MIN_TRAVEL_MONTH = 1.0
        feat2 = 2 * (float(result['travelMonth']-MIN_TRAVEL_MONTH)/(MAX_TRAVEL_MONTH-MIN_TRAVEL_MONTH)) - 1
        #==========================================================================
        
        #=========feature=three====source=latitude=================================
        MAX_LATITUDE = 71.5
        MIN_LATITUDE = 27.0    
        #sources : (i)https://www.google.de/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=lisbon%20longitude%20and%20latitude
        #sources : (ii)http://earthquake.usgs.gov/hazards/apps/vs30/predefined.php  *************NICE*************
        feat3 = 2 * (float(result['sourceLatitude'] - MIN_LATITUDE)/(MAX_LATITUDE-MIN_LATITUDE)) - 1
        #==========================================================================

        #=========feature=four====source=longitude=================================
        MAX_LONGITUDE = 42.5
        MIN_LONGITUDE = -10.0
        feat4 = 2 * (float(result['sourceLongitude'] - MIN_LONGITUDE)/(MAX_LONGITUDE - MIN_LONGITUDE)) - 1
        #==========================================================================
        
        #=========feature=five======destination=latitude===========================
        feat5 = 2 * (float(result['destinationLatitude'] - MIN_LATITUDE)/(MAX_LATITUDE - MIN_LATITUDE)) - 1
        #==========================================================================
    
        #=========feature=six======destination=longitude===========================
        feat6 = 2 * (float(result['destinationLongitude'] - MIN_LONGITUDE)/(MAX_LONGITUDE - MIN_LONGITUDE)) - 1
        #==========================================================================
    
        #=========feature=seven====search=travel=timestamp=difference==============
        MAX_TIMESTAMP_DIFFERENCE = 15552000.0
        #this corresponds to six months in terms of seconds
        feat7 = 2 * (float(result['travelTimestamp']-result['searchTimestamp'])/MAX_TIMESTAMP_DIFFERENCE) - 1
        #==========================================================================
    
        #=========feature=eight======seats=left====================================
        MAX_SEATS = 7.0
        #assumption that no one will offer more than 8 seats
        MIN_SEATS = 1.0
        feat8 = 2 * (float(result['seatsLeft']-MIN_SEATS)/(MAX_SEATS-MIN_SEATS)) - 1
        #==========================================================================
        
        #========output=one=======availablility====================================
        out1 = result['resultAvailability']
        #==========================================================================
        
        #========output=two==========price=========================================
        out2 = result['resultPrice']
        #==========================================================================
    
    
    
        #============append=to=train_X=train_Y_avail=train_Y_price=================
        train_X.append([feat1, feat2, feat3, feat4, feat5, feat6, feat7, feat8])
        train_Y_avail.append(out1)
        train_Y_price.append(out2)
        #==========================================================================

        
    #return the input X, and the two kind of outputs/ Y's
    return train_X, train_Y_avail, train_Y_price



    