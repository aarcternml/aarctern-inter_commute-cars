# -*- coding: utf-8 -*-
"""
Created on Mon Sep 07 21:22:07 2015

@author: Ashish
"""

from lxml import html
import requests
import json
import os
import numpy as np
from numpy import arange
from parse import *
import parse
import re
from datetime import datetime
import time
import numpy as np
from json import dumps, load
from datetime import timedelta
from collections import OrderedDict

#6 months limit FOR NOW
TRAVEL_SEARCH_DIFF_LIMIT = 15552000

for_comma = 0

#this dictionary os to get the latitude and longitude of the places
latlongdict = OrderedDict(
[('Amsterdam' ,    [52.370216  ,   4.895168]),

('Zurich' ,       [47.376887  ,   8.541694]),

('Paris' ,        [48.856614  ,   2.352222]),

('Rome' ,         [41.902783  ,  12.496366]),

('Milan' ,        [45.465422  ,   9.185924]),

('Brussel' ,      [50.85034   ,   4.35171]),

('Luxembourg' ,   [49.611621  ,   6.131935]),

('Prague' ,       [50.075538  ,  14.4378]),

('Vienna' ,       [48.208174  ,  16.373819]),

('Warsaw' ,       [52.229676  ,  21.012229]),

('Berlin' ,       [52.520007  ,  13.404954]),

('Hamburg' ,      [53.551085  ,   9.993682]),

('Leipzig' ,      [51.339695  ,  12.373075]),

('Dresden' ,      [51.050409  ,  13.737262]),

('Nuremberg' ,    [49.45203  ,   11.07675]),

('Munich' ,       [48.135125  ,  11.581981]),

('Stuttgart' ,    [48.775846  ,   9.182932]),

('Frankfurt' ,    [50.110922  ,   8.682127]),

('Dusseldorf' ,   [51.227741  ,   6.773456]),

('Hannover' ,     [52.375892  ,   9.73201])]
)


#this dictionary is to facilitate the conversion from place names to their url portions in blablacar.de
urldict = {'hannover' : ['Hannover&', '52.375892%7C9.73201&', 'DE&'],               
               'duesseldorf' : ['D%C3%BCsseldorf&', '51.227741%7C6.773456&', 'DE&'],
               'frankfurt-am-main' : ['Frankfurt', '50.110922%7C8.682127&', 'DE&'],
               'stuttgart' : ['Stuttgart&' ,'48.775846%7C9.182932&', 'DE&'],
               'muenchen' : ['munich&', '48.135125%7C11.581981&', 'DE&'],
               'nuernberg' : ['N%C3%BCrnberg&', '49.45203%7C11.07675&', 'DE&'],
               'dresden' : ['Dresden&', '51.050409%7C13.737262&', 'DE&'],
               'leipzig' : ['Leipzig&', '51.339695%7C12.373075&', 'DE&'],
               'berlin' : ['berlin&', '52.5200066%7C13.404954&', 'DE&'],
               'hamburg' : ['Hamburg&', '53.551085%7C9.993682&', 'DE&'],
               'warschau' : ['Warschau%2C+Polen&', '52.229676%7C21.012229&', 'PL&'],
               'prag' : ['Prag&', '50.075538%7C14.4378&', 'CZ&'],
               'wien' : ['Wien%2C+%C3%96sterreich&', '48.208174%7C16.373819&', 'AT&'],
               'rom' : ['Rome%2C+Rom%2C+Italien&', '41.902783%7C12.496366&', 'IT&'],
               'mailand' : ['Mailand%2C+Italien&', '45.465422%7C9.185924&', 'IT&'],
               'zuerich' : ['Z%C3%BCrich%2C+Schweiz&', '47.376887%7C8.541694&', 'CH&'],
               'paris' : ['paris&', '48.856614%7C2.352222&', 'FR&'],
               'bruessel' : ['Br%C3%BCssel%2C+Belgien&', '50.85034%7C4.35171&', 'BE&'],
               'luxemburg' : ['Luxemburg%2C+Luxemburg&', '49.611621%7C6.131935&', 'LU&'],
               'amsterdam' : ['Amsterdam%2C+Niederlande&', '52.370216%7C4.895168&', 'NL&']
               }


#this dictionary is to facilitate conversion from natural language names to blablacar's naming of cities
citynamedict = {'Hannover':'hannover', 'Dusseldorf' : 'duesseldorf', 'Frankfurt' : 'frankfurt-am-main',
                'Stuttgart' : 'stuttgart', 'Munich' : 'muenchen', 'Nuremberg' : 'nuernberg',
                'Dresden' : 'dresden', 'Leipzig' : 'leipzig', 'Berlin' : 'berlin', 
                'Hamburg' : 'hamburg', 'Warsaw' : 'warschau', 'Prague' : 'prag',
                'Vienna' : 'wien', 'Rome' : 'rom', 'Milan' : 'mailand',
                'Zurich' : 'zuerich', 'Paris' : 'paris', 'Brussel' : 'bruessel',
                'Luxembourg' : 'luxemburg', 'Amsterdam' : 'amsterdam'}


#this dictionary is for German months to numbers
monthdict = {'Januar':1, 
             'Februar':2,
             'Marz':3,
             'M\xe4rz':3,
             'M\xc3\xa4rz':3,
             'April':4,
             'Mai':5,
             'Juni':6,
             'Juli':7,
             'August':8,
             'September':9,
             'Oktober':10,
             'November':11,
             'Dezember':12}
             
dayofweekdict = {'Montag':1,
                 'Dienstag':2,
                 'Mittwoch':3,
                 'Donnerstag':4,
                 'Freitag':5,
                 'Samstag':6,
                 'Sonntag':7}
                 

#OPEN A JSON FILE TO WRITE ALL THE CONTENTS ABOUT TO BE SCRAPED HERE
filepath = '/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/data/' + time.strftime("%Y_%m_%d") + '.json'
fout = open(filepath,'wb')
#write the intial '[' to start the list
fout.write('[\n')

#page = requests.get('https://www.blablacar.fr/trajets/paris/toulouse/#?fc=48.856614%7C2.352222&fcc=FR&tc=43.604652%7C1.444209&tcc=FR&db=25%2F09%2F2015&sort=trip_date&order=asc&limit=10&page=1')
#page = requests.get('https://www.blablacar.fr/search_xhr?fn=paris&fc=48.856614%7C2.352222&fcc=FR&tn=isle&tc=45.805176%7C1.226734&tcc=FR&db=7%2F10%2F2015&sort=trip_date&order=asc&limit=1000&page=1')
#page = requests.get('https://www.blablacar.de/search_xhr?fn=berlin&fc=52.520007%7C13.404954&fcc=DE&tn=hamburg&tc=53.551085%7C9.993682&tcc=DE&sort=trip_date&order=asc&limit=1000&page=1')
#tree = html.fromstring(page.text)


#d =json.loads(page.text)

#htmlstr = d['html']['results']
#tree = html.fromstring(htmlstr)

#for  nth driver,
#n = 0


#availabilities
#print len(tree[4])

#time of departure
#print tree[4][n][0][1][1][0].text

#price
#print tree[4][n][0][1][2][0][0][0].text

#places left
#print tree[4][n][0][1][2][1][0].text


#Format of each entry of the json data
#{"results":[],"sourceName":"Paris","sourceLatitude":"48.833333","sourceLongitude":"2.333333",
#"destinationname":"Paris","destinationLatitude":"48.833333","destinationLongitude":"2.333333",
#"searchDate":20,"searchMonth":9,"searchYear":2015,"searchDay":"Sunday","searchTimestamp":1442734693448,
#"travelDate":21,"travelMonth":9,"travelYear":2015,"travelDay":"Monday","travelTimestamp":1442773800000,
#"resultCount":0}

#location of the folder where all json data is to be stored

for fromcity in latlongdict.keys():
    for tocity in latlongdict.keys():
        
        #get the source and dest lat and long using dict
        sourceLatitude = latlongdict[fromcity][0]
        sourceLongitude = latlongdict[fromcity][1]
        destinationLatitude = latlongdict[tocity][0]
        destinationLongitude = latlongdict[tocity][1]
        
        #get the source and dest city names
        sourceName = fromcity
        destinationName = tocity
        
        #get the searching date and time
        searchdt = datetime.utcnow()
        searchDate = searchdt.day
        searchMonth = searchdt.month
        searchYear = searchdt.year
        searchDay = searchdt.isoweekday()
        zerotimestamp = datetime.fromtimestamp(0)
        searchTimestamp = int((searchdt - zerotimestamp).total_seconds())
        
        #NO SPECIFIC TRAVEL DATES since all entries until next 6 months taken into account
        
  
        #MAIN BUSINESS : GENERATE THE URL and GET RESULTS
        sourceUrl = urldict[citynamedict[fromcity]]
        destinationUrl = urldict[citynamedict[tocity]]
        search_query = 'https://www.blablacar.de/search_xhr?fn=' + sourceUrl[0] + 'fc=' + sourceUrl[1] + 'fcc=' + sourceUrl[2] + 'tn=' + destinationUrl[0] + 'tc=' + destinationUrl[1] + 'tcc=' + destinationUrl[2] + 'sort=trip_date&order=asc&limit=1000&page=1'

        #request the contents of this search page
        page = requests.get(search_query)
        
        #get the json contents
        data =json.loads(page.text)

        #take out the results component from the search results page
        htmlstr = data['html']['results']
        
        #form an lxml tree
        tree = html.fromstring(htmlstr)
        
        #availabilities
        #print len(tree[4])
        #for  nth driver,        
        #time of departure
        #print tree[4][n][0][1][1][0].text
        #price
        #print tree[4][n][0][1][2][0][0][0].text
        #places left
        #print tree[4][n][0][1][2][1][0].text
        
        
        #IF LENGTH OF TREE IS LESS THAN 5, IGNORE/CONTINUE WITH THE NEXT CITY PAIR
        #THIS WILL HAPPEN WHEN fromcity == tocity
        if (len(tree)<5):
            continue
        
        #number of results on the page (number of offers/drivers)
        resultCount = len(tree[4])
        
        #initialize the three result lists : seats left, dep. time, price
        resultTimeOfDeparture = []
        resultPrice = []
        resultSeatsLeft = []
        
        
        #LOOP FOR ALL ENTRIES ON THE SEARCH RESULTS PAGE
        
        for i in range(resultCount):                             
                        
            #========TIME=OF=DEPARTURE======================
            #extract the tod text in format : "Freitag 16. Oktober - 15:00 Uhr"
            todTextRaw = tree[4][i][0][1][1][0].text
            #split the text into components 
            #like    ['Freitag', '16.', 'Oktober', '-', '15:00', 'Uhr']  
            #or like ['Heute', '-', '16:00', 'Uhr'] 
            #or like ['Freitag', '16.', 'Oktober', '2016', '-', '15:00', 'Uhr'] 
            todTextSplit = todTextRaw.split()
            #get the individual travel date and time components            
            
            #ENTRIES FOR NEXT YEAR
            if (len(todTextSplit)==7):
                travelDay = dayofweekdict[todTextSplit[0]]
                travelDate = int(todTextSplit[1][:-1])
                travelMonth = monthdict[todTextSplit[2].encode('utf-8')]            
                travelYear = int(todTextSplit[3])
                travelHour = int(todTextSplit[5][:-3])
                
            else:
                #handle 'Heute and Morgen separately'            
                #CASE 1 : NEITHER 'Heute' NOR 'Morgen'
                if ((todTextSplit[0] != 'Heute') and (todTextSplit[0] != 'Morgen')):
                    travelDay = dayofweekdict[todTextSplit[0]]
                    travelDate = int(todTextSplit[1][:-1])
                    travelMonth = monthdict[todTextSplit[2]]            
                    if travelMonth>=searchMonth:
                        travelYear = searchYear
                    else:
                        travelYear = searchYear + 1
                    travelHour = int(todTextSplit[4][:-3])   
                #CASE 2 : HEUTE        
                elif (todTextSplit[0] == 'Heute'):
                    travelDay = searchDay
                    travelDate = searchDate
                    travelMonth = searchMonth           
                    travelYear =  searchYear
                    travelHour = int(todTextSplit[2][:-3])
                #================================================================================
                #CASE3 : MORGEN
                elif (todTextSplit[0] == 'Morgen'):
                    delta = timedelta(days = 1)
                    traveldatetime = searchdt + delta
                    travelDay = traveldatetime.isoweekday()
                    travelDate = traveldatetime.day
                    travelMonth = traveldatetime.month           
                    travelYear =  traveldatetime.year                            
                    travelHour = int(todTextSplit[2][:-3])                        
                #===============================================================================
                    
            traveldt = datetime(year=travelYear, month=travelMonth, day=travelDate, hour=travelHour)
            travelTimestamp = int((traveldt - zerotimestamp).total_seconds())
            
            #check if this travel entry is within 6 months (i.e. ) 6*30*24*3600 = 15552000 secs
            #if NOT then do not proceed further
            diffTravelSearch = int((traveldt - searchdt).total_seconds())
            if diffTravelSearch >= TRAVEL_SEARCH_DIFF_LIMIT:
                continue
            
            #finally add this entry to resultTimeOfDeparture list
            #Currently just adding the DAY-WISE TIME STAMP SO THAT ALL ENTRIES OF A DAY CAN BE BUCKETED
            day_wise_time_stamp = int((datetime(year=travelYear, month=travelMonth, day=travelDate)-zerotimestamp).total_seconds())
            #==================================================
            
            
            #============PRICE==============================
            #price in euros: exracted text
            priceTextRaw = tree[4][i][0][1][2][0][0][0].text            
            #regex to extract the number (in euros)
            priceRegex = re.search(r"\d+",priceTextRaw.encode('utf-8'))
            #store the final string number in priceText
            priceText = int(priceRegex.group())
            #===============================================
            
            
            #==========SEATS=LEFT===========================
            #extract the required text
            seatsleftTextRaw = tree[4][i][0][1][2][1][0].text
            #check if the entry is full, if so proceed to next entry
            if 'Ausgebucht'in seatsleftTextRaw:
                continue
            
            #otherwise get the string in the form of int
            seatsleftText = int(seatsleftTextRaw)
            #===============================================
            
            
            #===SAVE=TIMEOFDEPARTURE=PRICE=SEATSLEFT========
            resultTimeOfDeparture.append(day_wise_time_stamp)
            resultPrice.append(priceText)            
            resultSeatsLeft.append(seatsleftText)
            #===============================================

            
        
        #INSTEAD OF A SEPARATE PRE-PROCESSING STEP, PRE-PROCESS THESE i RESULTS HERE
        #TO SAVE A LOT OF SPACE IN SAVING JSON FILES
        
        #Before that, update the actual resultCount
        resultCountOrignal = resultCount
        resultCount = len(resultPrice)
        #cross check the size of the three page results lists
        assert len(resultPrice) == len(resultSeatsLeft)
        assert len(resultTimeOfDeparture) == len(resultSeatsLeft)
        
        #Given we have results in ascending order of date, we categorize the different travel days,
        #hence we need another set of 3 lists such that have resultTimeOfDeparture has unique days only,
        #Finally, within this loop, we can start writing to the json file which will have MXNXP entries,
        #where M = no. of source places, N = number of dest places, P = length of list : resultTimeOfDepartureUnique

        resultTimeOfDepartureUnique = list(np.unique(np.array(resultTimeOfDeparture), return_counts = True)[0])
        resultTimeOfDepartureUniqueCounts = list(np.unique(np.array(resultTimeOfDeparture), return_counts = True)[1])
        resultPricePerDay = []
        resultSeatsLeftPerDay = []
        #this is the count for dfferent days for which there are postings on website
        uniqueCount = len(resultTimeOfDepartureUnique)
        #START THE PRE-PROCESSING/PAGE-RESULTS-COMPRESSION/RESULTS-MERGING LOOP        
        counter = 0
        for j in range(uniqueCount):
            #run another sub loop for all postings for j^th date
            for k in range(resultTimeOfDepartureUniqueCounts[j]):
                #get all the entries first in lists resultPricePerDay and resultSeatsLeftPerDay
                resultPricePerDay.append(resultPrice[counter])
                resultSeatsLeftPerDay.append(resultSeatsLeft[counter])
                #increment the counter :that hovers over the bog lists : like resultPrice, etc.
                counter = counter + 1
                
            #now we have the  resultPricePerDay and resultSeatsLeftPerDay lists and resultTimeOfDepartureUnique[j] as that day's timestamp
            #NEXT : get number of UNIQUE SEATS LEFT
            resultSeatsLeftPerDayUnique = list(np.unique(np.array(resultSeatsLeftPerDay)))
            
            #run a loop over all unique values; inside the loop, get the indices where the current
            #loop value is the seats-left value, and merge all the results and write to the json
            
            #convert the list to np.array
            SeatsArray = np.array(resultSeatsLeftPerDay)
            PriceArray = np.array(resultPricePerDay)
            
            for uniqueSeatsLeft in resultSeatsLeftPerDayUnique:
                #it is ascending order starting from 1, 2, ... 5, 6 ..,
                #hence any number of seats >= 1 is good for f = 1, for eg.
                #get the binary vector of indices for which condition holds
                indVec = (SeatsArray >= uniqueSeatsLeft).astype(int)
                #eg. a = np.array([1,2,3]) | (a>=2).astype(int) | array([0, 1, 1])
                #NOW get the resultCount for 'f' number of seats
                
                
                #===CURRENTLY TAKING MEAN OF ALL PRICES, MAY DO SOMETHING ELSE HERE===
                resultCountFinal = np.sum(indVec)
                resultPriceFinal = float(np.sum(np.multiply(PriceArray,indVec))) / resultCountFinal
                #=========keep only 2 decimal places in resutPrice==========
                resPriceTemp = "%.2f" % resultPriceFinal
                resultPriceFinal = float(resPriceTemp)
                resultTravelDay = datetime.fromtimestamp(resultTimeOfDepartureUnique[j]).isoweekday()
                resultTravelMonth = datetime.fromtimestamp(resultTimeOfDepartureUnique[j]).month
                resultTravelDate = datetime.fromtimestamp(resultTimeOfDepartureUnique[j]).day
                resultTravelYear = datetime.fromtimestamp(resultTimeOfDepartureUnique[j]).year
                #=====================================================================
                
                #=============WRITE THESE THINGS TO THE JSON FILE==============
                if (for_comma==1):
                    fout.write('\n,\n')
                json.dump(OrderedDict([("resultAvailability",resultCountFinal), ("resultPrice",resultPriceFinal), 
                ("seatsLeft" , uniqueSeatsLeft), ("sourceName",sourceName),("sourceLatitude",sourceLatitude),
                ("sourceLongitude",sourceLongitude), ("destinationname",destinationName), 
                ("destinationLatitude",destinationLatitude),("destinationLongitude",destinationLongitude),
                ("searchDate",searchDate), ("searchMonth",searchMonth), ("searchYear",searchYear),
                ("searchDay",searchDay), ("searchTimestamp",searchTimestamp), ("travelDay", resultTravelDay),
                ("travelMonth", resultTravelMonth), ("travelDate", resultTravelDate), ("travelYear", resultTravelYear),
                ("travelTimestamp",resultTimeOfDepartureUnique[j])]), fout)
                #also append a comma with nextlines
                for_comma = 1
                
                
#add the ending ']' to the json file
fout.write('\n]')
#FINALLY, CLOSE THE JSON FILE
fout.close()                