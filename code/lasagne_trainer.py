# -*- coding: utf-8 -*-
"""
Created on Sun Oct 04 22:52:29 2015

@author: Ashish
"""
import sys
from lxml import html
import requests
import json
import os
import numpy as np
from numpy import arange
from parse import *
import parse
import re
from datetime import datetime
import time
import numpy as np
from json import dumps, load
from datetime import timedelta
from collections import OrderedDict
from sknn.mlp import Regressor, Layer
from pre_process_helper import normalize_features
import os.path
import pickle
import matplotlib
matplotlib.use('Agg')
from matplotlib.pyplot import *
import theano
import theano.tensor as T

import lasagne


NUM_EPOCHS = 100
BATCH_SIZE = 50
LRATE = 0.001
MOMENTUM = 0.9

#load the json file
filepath = '/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/data/' + time.strftime("%Y_%m_%d") + '.json'
fin = open(filepath,'r')

#get the json contents in an object
contents = json.load(fin)

#get the input, X and output, Y for the network
train_X, train_Y_avail, train_Y_price = normalize_features(contents)

#convert these lists to arrays
train_X = np.array(train_X)
train_Y_avail = np.array(train_Y_avail)
train_Y_price = np.array(train_Y_price)
train_Y_avail = np.reshape(train_Y_avail, [-1,1])

#NOW WE HAVE THE INPUT AND OUTPUT DATA FOR TRAINING THE NETWORK



def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]



#NEURAL=NETWORK=PART=BEGINS====================================================

#=================1. AVAILABILITY ========================================
#for the first time, create a new network
if (not(os.path.exists('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_avail_lasagne.pkl'))):
    
    input_var = T.dmatrix('inputs')
    target_var = T.dmatrix('targets')
    
    l_in = lasagne.layers.InputLayer(shape=(None, 8), input_var=input_var)
    l_hid1 = lasagne.layers.DenseLayer(l_in, num_units=50, nonlinearity=lasagne.nonlinearities.sigmoid)
    l_hid2 = lasagne.layers.DenseLayer(l_hid1, num_units=100, nonlinearity=lasagne.nonlinearities.sigmoid)
    l_out = lasagne.layers.DenseLayer(l_hid2, num_units=1, nonlinearity=lasagne.nonlinearities.linear)
    network = l_out
    
    prediction = lasagne.layers.get_output(network)
    loss = lasagne.objectives.squared_error(prediction, target_var)
    loss = loss.mean()
    
    params = lasagne.layers.get_all_params(network, trainable=True)
    updates = lasagne.updates.nesterov_momentum(loss, params, learning_rate = np.float32(LRATE), momentum=np.float32(MOMENTUM))
    
    test_prediction = lasagne.layers.get_output(network, deterministic=True)
    test_loss = lasagne.objectives.squared_error(test_prediction, target_var)    
    test_loss = test_loss.mean()
        
    train_fn = theano.function([input_var, target_var], loss, updates=updates)
    
    val_fn = theano.function([input_var, target_var],  test_loss)
    
    print("Starting training...")
    
    for epoch in range(NUM_EPOCHS):
        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(train_X[:-2100], train_Y_avail[:-2100], BATCH_SIZE):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1
        
        val_err = 0
        val_batches = 0
        for batch in iterate_minibatches(train_X[-2050:], train_Y_avail[-2050:], BATCH_SIZE):
            inputs, targets = batch
            err = val_fn(inputs, targets)
            val_err += err
            val_batches += 1
            
        print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, NUM_EPOCHS, time.time() - start_time))
        print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
        print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
        
    test_err = 0
    test_batches = 0
    for batch in iterate_minibatches(train_X[-2050:], train_Y_avail[-2050:], BATCH_SIZE):
        inputs, targets = batch
        err = val_fn(inputs, targets)
        test_err += err
        test_batches += 1
         
    print("Final results:")
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches)) 
    
    #save the network
    #pickle.dump(network, open('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_avail_lasagne.pkl', 'wb'))
    
    
    
else:
    
    network = pickle.load(open('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_avail_lasagne.pkl','rb'))

    input_var = T.dmatrix('inputs')
    target_var = T.dmatrix('targets')
    
    prediction = lasagne.layers.get_output(network)
    loss = lasagne.objectives.squared_error(prediction, target_var)
    loss = loss.mean()
    
    params = lasagne.layers.get_all_params(network, trainable=True)
    updates = lasagne.updates.nesterov_momentum(loss, params, learning_rate = np.float32(LRATE), momentum=np.float32(MOMENTUM))
    
    test_prediction = lasagne.layers.get_output(network, deterministic=True)
    test_loss = lasagne.objectives.squared_error(test_prediction, target_var)    
    test_loss = test_loss.mean()
        
    train_fn = theano.function([input_var, target_var], loss, updates=updates, on_unused_input='warn')
    
    val_fn = theano.function([input_var, target_var],  test_loss)
    
    print("Starting training...")
    
    for epoch in range(NUM_EPOCHS):
        train_err = 0
        train_batches = 0
        start_time = time.time()
        for batch in iterate_minibatches(train_X[:-1100], train_Y_avail[:-1100], BATCH_SIZE):
            inputs, targets = batch
            train_err += train_fn(inputs, targets)
            train_batches += 1
        
        val_err = 0
        val_batches = 0
        for batch in iterate_minibatches(train_X[-1050:], train_Y_avail[-1050:], BATCH_SIZE):
            inputs, targets = batch
            err = val_fn(inputs, targets)
            val_err += err
            val_batches += 1
            
        print("Epoch {} of {} took {:.3f}s".format(
            epoch + 1, NUM_EPOCHS, time.time() - start_time))
        print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
        print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
        
    test_err = 0
    test_batches = 0
    for batch in iterate_minibatches(train_X[-1050:], train_Y_avail[-1050:], BATCH_SIZE):
        inputs, targets = batch
        err = val_fn(inputs, targets)
        test_err += err
        test_batches += 1
         
    print("Final results:")
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches)) 
    
    #save the network
    pickle.dump(network, open('/home/ubuntu/AARCTERN/git/aarctern-inter_commute-cars/network/nn_avail_lasagne.pkl', 'wb'))

#=========================================================================               
                      
    




    